# Simple home control with tellstick-net #
This package is a wrapper on telldus-live, https://www.npmjs.com/package/telldus-live.

## Before Starting
You will need a Telldus Live account and OAuth tokens:

To get a Telldus Live account, go to login.telldus.com

Once you have a Telldus Live account, go to api.telldus.com and Generate a private token for my user only.

## Install
```
npm install tellstick-control
```

## Setup 

Edit this config file with your API credentials and routes (explained later). 
```
{
    "config": {
        "publicKey": "fsdfdafs",
        "privateKey": "sdfdsfsdf",
        "token": "sdfsdfs",
        "tokenSecret": "asdasdas"
    },
    "routes": {
        "coffee": "onOffDevice:4543",
        "list": "list"
    }
}
```

## Use it

With this settings you are able to run commands to trigger event with your Tellstick. 

There is two basic commands implemented now: onOffDevice and list. 
onOffDevice take the device id as parameter trigger and allow on and off event for your route. 

In this simple example I added the route coffee as a onOffDevice with id 4543, 
this make it possible att turn off the coffee maker with 

```
node index.js coffee/on
```
or 
```
node index.js coffee on
```

The other command is list, which give you a list of connected devices. Run this with 
```
node index.js list
```