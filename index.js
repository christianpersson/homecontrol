var TelldusAPI = require('telldus-live'),
    _ = require("lodash"),
    routes = require("./config.json").routes,
    config = require("./config.json").config,
    cloud, args;

cloud = new TelldusAPI.TelldusAPI({
    publicKey: config["publicKey"],
    privateKey: config["privateKey"]
}).login(config["token"], config["tokenSecret"], function(err, user) {
    if (!!err) return console.log('login error: ' + err.message);
}).on('error', function(err) {
    console.log('background error: ' + err.message);
});


var commands = function(cloud) {
    function list() {
        return function() {
            cloud.getDevices(function(err, devices) {
                if (!!err) return console.log('getDevices: ' + err.message);
                console.log(devices);
            });
        }
    }

    function onOffBase(device, status) {
        return function() {
            cloud.onOffDevice({
                id: device
            }, status, console.log);
        }
    }

    function onOffDevice(device) {
        return {
            on: onOffBase(device, true),
            off: onOffBase(device, false)
        }
    }

    return {
        list: list,
        onOffDevice: onOffDevice
    }

}(cloud);

_.forIn(routes, function(v, k) {
    var args = v.split(":");
    routes[k] = commands[args[0]](args.slice(1, 2));
});

args = process.argv.slice(2, Infinity);

if (args[0].indexOf("/") !== -1) {
    args = args[0].split("/");
}

function execute(routes, command) {
    if (command.length === 0) {
        return;
    }
    var next = routes[command.splice(0, 1)];

    if (typeof next === "function" && command.length === 0) {
        next();
    } else if (command.length > 0) {
        execute(next, command);
    }
}

execute(routes, args);